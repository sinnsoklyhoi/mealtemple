//
//  POSNotification.swift
//  Mealtemple
//
//  Created by GIS on 12/11/19.
//  Copyright © 2019 GIS. All rights reserved.
//
import UIKit

public enum POSNotificationType {
    case success
    case error
    case info
    
    var color: UIColor {
        switch self {
        case .success: return .posGreen
        case .error: return .posRed
        case .info: return .posGray
        }
    }
    
    var icon: String {
        switch self {
        case .success: return "notif_success"
        case .error: return "notif_error"
        case .info: return "notif_info"
        }
    }
    
}

class POSNotification: UIView {
    
    private var type:POSNotificationType
    private static var current = [POSNotification]()
    
    init(type:POSNotificationType, frame: CGRect) {
        self.type = type
        super.init(frame:frame)
        
        layer.cornerRadius = 5
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.4
        layer.shadowColor = UIColor.lightGray.cgColor
        backgroundColor = type.color
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public static func showNotification(type: POSNotificationType, title: String, message: String) {
        guard let window = UIApplication.shared.keyWindow else {
            print("Failed to show CRNotification. No keywindow available.")
            return
        }
        if current.count > 10 {
            return
        }
        
        let titleFont = UIFont.boldSystemFont(ofSize: 15)
        let textFont = UIFont.systemFont(ofSize: 13)
        let width : CGFloat = window.frame.width
        let margin : CGFloat = 12
        let iconWidth : CGFloat = 30
        let titleHeight = title.height(withConstrainedWidth: width - iconWidth - margin*3, font: titleFont)
        let textHeight = message.height(withConstrainedWidth: width - iconWidth - margin*3, font: textFont)
        let totalHeight = margin*2 + max(iconWidth, titleHeight + textHeight)
        let notif = POSNotification(type:type, frame: CGRect(x:10, y:-totalHeight, width:width-20, height:totalHeight))
        
        let icon = UIImageView(frame:CGRect(x:margin, y:margin, width:iconWidth, height:iconWidth))
        icon.image = UIImage(named:type.icon)?.withRenderingMode(.alwaysTemplate)
        icon.tintColor = .white
        notif.addSubview(icon)
        
        let titleLbl = UILabel(frame:CGRect(x:margin*2 + iconWidth, y:margin, width:width - margin*3 - iconWidth, height:titleHeight))
        titleLbl.font = titleFont
        titleLbl.text = title
        titleLbl.textColor = .white
        titleLbl.numberOfLines = 0
        notif.addSubview(titleLbl)
        
        let textLbl = UILabel(frame:CGRect(x:margin*2 + iconWidth, y:margin + titleHeight, width:width - margin*3 - iconWidth, height:textHeight))
        textLbl.font = textFont
        textLbl.text = message
        textLbl.textColor = .white
        textLbl.numberOfLines = 0
        notif.addSubview(textLbl)
        
        let gr = UISwipeGestureRecognizer(target:self, action: #selector(swipedRight(_:)))
        gr.direction = .right
        notif.addGestureRecognizer(gr)
        
        current.insert(notif, at: 0)
        window.addSubview(notif)
        updatePositions()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            current.removeFirstEqual(notif)
            UIView.animate(withDuration: 0.2, animations: {
                notif.alpha = 0
            }, completion: { (success) in
                notif.removeFromSuperview()
            })
            
            
        }
    }
    
    @objc static func swipedRight(_ gr:UISwipeGestureRecognizer) {
        let notif = gr.view as! POSNotification
        current.removeFirstEqual(notif)
        let target = CGRect(x:notif.frame.origin.x + notif.frame.width + 16, y:notif.frame.origin.y, width:notif.frame.width, height:notif.frame.height)
        UIView.animate(withDuration: 0.2, animations: {
            notif.frame = target
        }, completion: { (success) in
            notif.removeFromSuperview()
            self.updatePositions()
        })
    }
    
    private static func updatePositions() {
        var y : CGFloat = 48
        for notif in current {
            let target = CGRect(x:notif.frame.origin.x, y:y, width:notif.frame.width, height:notif.frame.height)
            UIView.animate(withDuration: 0.2) {
                notif.frame = target
            }
            y += notif.frame.height + 16
        }
    }
}

