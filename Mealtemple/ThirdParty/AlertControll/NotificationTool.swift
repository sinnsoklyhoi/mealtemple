//
//  NotificationTool.swift
//  Mealtemple
//
//  Created by GIS on 12/11/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class NotificationTool: NSObject {
    
    public static func showError(message: String) {
        POSNotification.showNotification(type: .error, title: "Error", message: message)
    }
    
    public static func showSuccess(message: String) {
        POSNotification.showNotification(type: .success, title: "Success", message: message)
    }
}
