//
//  TextFieldPadding.swift
//  LEESHOP
//
//  Created by GIS on 8/16/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit

class UITextfieldPadding: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
