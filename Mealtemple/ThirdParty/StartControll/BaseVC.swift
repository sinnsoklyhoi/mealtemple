//
//  BaseVC.swift
//  MobikulMagento-2
//
//  Created by Sinn soklyhoi on 12/25/19.
//  Copyright © 2019 kunal. All rights reserved.
//

import UIKit
class BaseVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.setTitle()
        self.navigationItem.rightBarButtonItems = []
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        var items : [UIBarButtonItem] = []
        for i in ["list","clock","new","discount","like","user"] {
            let barButton = UIBarButtonItem(image: UIImage(named:i)?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(test))
            barButton.tintColor = .white
            barButton.width = self.view.frame.width/6 - 5
            barButton.tag = 1
            items.append(barButton)
        }
        self.navigationItem.leftBarButtonItems = items
    }
    
    @objc func test() {}
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    internal func setTitle() -> String {
        return ""
    }
    
    public var getHeightNav : CGFloat {
        return /*UIApplication.shared.statusBarFrame.size.height +*/ (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    public var getWidthNav : CGFloat {
        return /*UIApplication.shared.statusBarFrame.size.height +*/ (self.navigationController?.navigationBar.frame.width ?? 0.0)
    }
    
    public var getStatusBarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height
    }
    
    public var getHeightTabBar : CGFloat {
        return self.tabBarController?.tabBar.frame.height ?? 0.0
    }
}
