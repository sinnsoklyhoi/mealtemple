//
//  WSConfig.swift
//  Mealtemple
//
//  Created by GIS on 12/16/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class WSConfig: NSObject {
    let BASE_URL = AppManager.environment() == .Dev ? "http//dev.com" : "https//mealtemple.com"
    let USER_NAME = ""
    let PASSWORD = ""
}
