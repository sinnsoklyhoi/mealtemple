//
//  BaseSV.swift
//  MOS TENH
//
//  Created by GIS on 11/29/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class BaseDAO: BaseWS ,URLSessionDelegate,DictionaryRepresentable{
    
    private var data : [[String:Any]] = []
    
    private func doProccess() {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        let urlString = self.fullUrl().addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let urlResquest = NSMutableURLRequest(url: URL(string: urlString!)!)
        urlResquest.httpMethod = "\(method())"
        
        var stringJson : String? {
            do {
                let _data = try JSONSerialization.data(withJSONObject: self.parame(), options: .init(rawValue: 0))
                return String(data: _data, encoding: String.Encoding.utf8)
            } catch {
                return nil
            }
        }
        
        if self.method() != .GET {
            urlResquest.setValue(isNeedAuthorization(), forHTTPHeaderField: "Authorization")
            urlResquest.httpBody = stringJson?.data(using: String.Encoding.utf8)
            urlResquest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        session.dataTask(with: urlResquest as URLRequest) { (data , request , errorData) in
            if let err = errorData {
                self.getError(error: err)
                return
            } else {
                do {
                    let resultData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    if let result = resultData as? [String : Any] {
                        if let data = result["data"] as? [[String:Any]] {
                            self.data = data
                        }
                        if let error = result["error"] as? Error {
                            self.getError(error: error)
                        }
                    }
                } catch let err {
                    self.getError(error: err)
                    return
                }
            }
        }.resume()
    }
    
    private func fullUrl() -> String {
        return BASE_URL + setUrl()
    }
    
    public func getCache() -> [DBObject] {
        self.doProccess()
        var obj: [DBObject] = []
        for data in data {
            obj.append(mapDictionaryInner(data))
        }
        return obj
    }
    
    public func getCache(byId: Int) -> DBObject {
        return DBObject()
    }
    
    public func update(obj: DBObject) {
        
    }
    
    public func insert(obj: DBObject) {
        
    }
    
    private func mapDictionnary(_ dict:[String:Any]) -> DBObject {
        let obj = mapDictionaryInner(dict)
        obj.id = dict["id"] as? Int
        obj.deletedAt = dict["deleted_at"] as? Date
        obj.createdAt = (dict["created_at"] as? Date)!
        return obj
    }
    
    internal func mapDictionaryInner(_ dict:[String:Any]) -> DBObject {
        return DBObject()
    }
    
    public func objToDict(obj:DBObject) -> [String:Any] {
        return [:];
    }
}

protocol DictionaryRepresentable {
    func dictionary() -> [String: Any]
}
extension DictionaryRepresentable {
    func dictionary() -> [String: Any] {
        let mirror = Mirror(reflecting: self)
        
        var dict: [String: Any] = [:]
        
        for (_, child) in mirror.children.enumerated() {
            if let label = child.label {
                dict[label] = child.value
            }
        }
        return dict
    }
}
