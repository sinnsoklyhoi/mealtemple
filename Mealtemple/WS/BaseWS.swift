//
//  BaseSW.swift
//  MOS TENH
//
//  Created by GIS on 11/29/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class BaseWS: WSConfig {
    
    enum Method {
        case POST
        case GET
        case DELETE
    }
    
    internal func getDAO() -> DBObject {
        return DBObject()
    }
    
    internal func parame() -> [String:Any] {
        return [:]
    }
    
    internal func method() -> Method {
        return .POST
    }
    
    internal func isNeedAuthorization() -> String{
        return ""
    }
    
    internal func setUrl() -> String {
        return ""
    }
    
    internal func getError(error: Error) {
        //
    }
}
