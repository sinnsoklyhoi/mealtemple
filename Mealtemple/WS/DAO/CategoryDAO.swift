//
//  CategoryDAO.swift
//  Mealtemple
//
//  Created by GIS on 12/16/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class CategoryDAO: BaseDAO {
    override func getError(error: Error) {
        //
    }
    
    override func setUrl() -> String {
        return ""
    }
    
    override func method() -> BaseWS.Method {
        return .POST
    }
    
    override func mapDictionaryInner(_ dict: [String : Any]) -> DBObject {
        let cat = Category()
        cat.icon = dict["icon"] as? String
        cat.name = dict["name"] as? String
        return cat
    }
    
    override func objToDict(obj: DBObject) -> [String : Any] {
        let cat = obj as! Category
        return [
            "icon" : cat.icon ?? NSNull(),
            "name" : cat.name ?? NSNull()
        ]
    }
}
