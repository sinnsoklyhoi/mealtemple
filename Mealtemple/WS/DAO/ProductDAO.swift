//
//  ProductDAO.swift
//  Mealtemple
//
//  Created by GIS on 12/18/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class ProductDAO: BaseDAO {
    
    override func objToDict(obj: DBObject) -> [String : Any] {
        let product = obj as! Product
        return [
            "name" : product.name!,
            "price" : product.price!
        ]
    }
    
    override func mapDictionaryInner(_ dict: [String : Any]) -> DBObject {
        let product = Product()
        product.name = dict["name"] as? String
        product.price = dict["price"] as? Double
        return product
    }
}
