//
//  CartDAO.swift
//  Mealtemple
//
//  Created by GIS on 12/18/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class CartDAO: BaseDAO {
    
    override func mapDictionaryInner(_ dict: [String : Any]) -> DBObject {
        let cart = Cart()
        cart.productIds = dict["product_id"] as? [Int] ?? []
        return cart
    }
    
    override func objToDict(obj: DBObject) -> [String : Any] {
         let cart = obj as! Cart
        return [
            "product_id" : cart.productIds
        ]
    }
}
