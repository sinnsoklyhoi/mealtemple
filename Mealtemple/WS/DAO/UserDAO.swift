//
//  UserDAO.swift
//  Mealtemple
//
//  Created by GIS on 12/16/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class UserDAO: BaseDAO {
    
    override func setUrl() -> String {
        return ""
    }
    
    override func getError(error: Error) {
        //
    }
    
    override func method() -> BaseWS.Method {
        return .POST
    }
    
    override func mapDictionaryInner(_ dict: [String : Any]) -> DBObject {
        let user = User()
        user.address = dict["address"] as? String
        user.email = dict["email"] as? String
        return user
    }
    
    override func objToDict(obj: DBObject) -> [String : Any] {
        let user = obj as! User
        return [
            "address" : user.address ?? NSNull(),
            "email" : user.email ?? NSNull()
        ]
    }
}
