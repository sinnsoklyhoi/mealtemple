//
//  AppDelegate.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    private var locationManager = CLLocationManager()
    private var centerMapCoordinate: CLLocationCoordinate2D!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UserSession().currentLange = Localize.Language.ENGLISH.rawValue
        GMSServices.provideAPIKey("AIzaSyAJMgEQNHbGfShs9-yM1FlYr7u1ivbo22o")
        GMSPlacesClient.provideAPIKey("AIzaSyAJMgEQNHbGfShs9-yM1FlYr7u1ivbo22o")
        self.getCurrentLocationOfUser()
        self.createTabBarController()
        return true
    }
    
    private func getCurrentLocationOfUser() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    public func createTabBarController() {
        
        let tabBarController = UITabBarController()
        let home = HomeVC()
        let cart = MyOrderVC()
        let profile = ProfileVC()
        let controller = [UINavigationController(rootViewController: home) , UINavigationController(rootViewController: cart) , UINavigationController(rootViewController: profile)]
        
        let homeIcon = UIImage(named: "home")?.withRenderingMode(.alwaysTemplate)
        let cartIcon = UIImage(named: "history")?.withRenderingMode(.alwaysTemplate)
        let profileIcon = UIImage(named: "profile")?.withRenderingMode(.alwaysTemplate)
        
        home.tabBarItem = UITabBarItem(title: "Home", image: homeIcon, tag: 0)
        cart.tabBarItem = UITabBarItem(title: "My Order", image: cartIcon, tag: 1)
        profile.tabBarItem = UITabBarItem(title: "Profile", image: profileIcon, tag: 2)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.posGreen], for: UIControl.State.selected)
        UITabBar.appearance().tintColor = .posGreen
        
        tabBarController.viewControllers = controller
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = tabBarController
        window?.tintColor = .posRed
        window?.makeKeyAndVisible()
    }
    
    func changeRootView(root : UITabBarController) -> Void {
        self.window?.rootViewController = root
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        self.locationManager.stopUpdatingLocation()
        self.centerMapCoordinate = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)! , longitude: (location?.coordinate.longitude)!)
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(centerMapCoordinate) { response, error in
            DispatchQueue.main.async {
                let address = response?.firstResult()
                let lines = address?.lines
                UserSession().currentLocationString = lines?.joined(separator: " , ")
                UserSession().currentLocationLatLng = ["\((location?.coordinate.latitude)!)",
                                                       "\((location?.coordinate.longitude)!)"].joined(separator: ",")
            }
        }
    }
}

