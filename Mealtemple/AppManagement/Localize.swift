//
//  Localize.swift
//  Mealtemple
//
//  Created by GIS on 12/19/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class Localize: NSObject {
    public enum Language : String {
        case ENGLISH = "en"
        case CHINESE = "zh-Hans"
    }
    
    private static func getDict(lang:String) -> NSDictionary {
        let bundle = Bundle.main.path(forResource: "Localizable", ofType: "strings", inDirectory: nil, forLocalization: lang)
        return NSDictionary(contentsOfFile: bundle!)!
    }
    
    public static func get(_ key:String) -> String {
        let currentLang = UserSession().currentLange ?? Language.ENGLISH.rawValue
        let dict = getDict(lang:currentLang)
        if let value = dict.value(forKey: key) {
            return value as! String
        } else {
            return key
        }
    }
}
