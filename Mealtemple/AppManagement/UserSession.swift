//
//  UserSession.swift
//  Mealtemple
//
//  Created by GIS on 12/18/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class UserSession: NSObject {
    
    public var isLogined: Bool! {
        set { UserDefaults.standard.set(newValue, forKey: "IS_USER_LOGINED")}
        get {UserDefaults.standard.bool(forKey: "IS_USER_LOGINED")}
    }
    
    public var token: String! {
        set { UserDefaults.standard.set(newValue, forKey: "USER_TOKEN")}
        get {UserDefaults.standard.string(forKey: "USER_TOKEN")}
    }
    
    public var currentLange: String! {
        set { UserDefaults.standard.set(newValue, forKey: "CURRENT_LANG")}
        get {UserDefaults.standard.string(forKey: "CURRENT_LANG")}
    }
    
    public var currentLocationString: String! {
        set { UserDefaults.standard.set(newValue, forKey: "CURRENT_LOCATION_STRING")}
        get {UserDefaults.standard.string(forKey: "CURRENT_LOCATION_STRING")}
    }
    
    public var currentLocationLatLng: String! {
        set { UserDefaults.standard.set(newValue, forKey: "CURRENT_LOCATION_LAT_LNG")}
        get {UserDefaults.standard.string(forKey: "CURRENT_LOCATION_LAT_LNG")}
    }
}
