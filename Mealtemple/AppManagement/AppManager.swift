//
//  AppManager.swift
//  Mealtemple
//
//  Created by GIS on 12/19/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class AppManager: NSObject {
    
    public enum AppEnvironment : String {
        case Production
        case Dev
    }
    
    public static func environment() -> AppEnvironment {
    #if DEBUG
        return .Production
    #else
        return .Dev
    #endif
    }
    
    public static func getCountryName(code:String?) -> String {
        if code == nil {
            return ""
        }
        let country = AppManager.countries().first{$0.code == code?.uppercased()}
        return country?.name ?? ""
    }
    
    public static func countries() -> [(code:String, name:String)] {
        if let path = Bundle.main.path(forResource: "countries", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let jsonResult = jsonResult as? [[String:String]]{
                    // do stuff
                    var list : [(code:String, name:String)] = []
                    for row in jsonResult {
                        list.append((code:row["code"]!, name:row["name"]!))
                    }
                    return list
                }
            } catch {
                // handle error
            }
        }
        return []
    }
    
    public static func timezones() -> [(code:String, name:String)] {
        if let path = Bundle.main.path(forResource: "timezone", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let jsonResult = jsonResult as? [[String:String]]{
                    // do stuff
                    var list : [(code:String, name:String)] = []
                    for row in jsonResult {
                        list.append((code:row["zone"]!, name:row["gmt"]!))
                    }
                    return list
                }
            } catch {
                // handle error
            }
        }
        
        return []
    }
    
    public static func getLanguage() -> String {
        if UserSession().currentLange == "en" {
            return "English"
        } else if UserSession().currentLange == "zh-Hans" {
            return "Chiness"
        } else {
            return ""
        }
    }
}
