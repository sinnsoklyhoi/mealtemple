//
//  User.swift
//  Mealtemple
//
//  Created by GIS on 12/11/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class User: DBObject {
    var name: String!
    var address: String!
    var email: String!
    var phone: String!
    var profileUrl: String!
    
    
}
