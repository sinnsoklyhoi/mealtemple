//
//  Restaurant.swift
//  Mealtemple
//
//  Created by GIS on 12/17/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class Restaurant: DBObject {
    var name: String!
    var profile: String?
    var cover: String?
    var rating: Double?
    var menus: [Menu] = []
    var isActive: Bool = false
    var distance: String!
    var feeDelivery: Double!
    
    public func getDistanceFormat() -> String {
        return distance + " km"
    }
    
    public func getFeeDelivery() -> String {
        return "Delivery Fee \(feeDelivery!)$"
    }
}
