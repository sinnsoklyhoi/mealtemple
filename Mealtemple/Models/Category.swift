//
//  Category.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class Category: DBObject {
    var name: String!
    var icon: String!
    
    override func asString() -> String {
        return Localize.get(name)
    }
}
