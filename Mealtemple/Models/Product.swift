//
//  Product.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation

class Product: DBObject {
    var name: String!
    var productImage: String!
    var price: Double!
    var quality: Int = 0
    var menuId: Int! = 0
    var comment: String?
    var rating: Double?
    var sold: Int?
    var disPrice: Double?
    
    var type: ProductType = .NEAREST
    
    enum ProductType: String {
        case NEAREST = "The Nearest"
        case BEST_SALE = "Best Seller"
        case TOP_RATE = "Top Rated"
        case POPULAR = "Most Popular"
    }
    
    public func allProductType() -> [ProductType] {
        return [.NEAREST,.BEST_SALE,.TOP_RATE,.POPULAR]
    }
    
    public func formatToString(price: Double) -> String {
        return "$ " + String(format: "%.2f", price)
    }
    
    public func getSalePrice() -> String {
        var afterDis: Double?
        if disPrice != nil {
            afterDis = price - (disPrice ?? 0)
        }
        if afterDis != nil {
            return formatToString(price: price - (disPrice ?? 0.0))
        }
        return formatToString(price: price)
    }
}
