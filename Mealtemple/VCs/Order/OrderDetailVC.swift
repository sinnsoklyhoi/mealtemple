//
//  OrderDetailVC.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/6/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class OrderDetailVC: BaseTableVC {
    
    override func setTitle() -> String {
        return "Order Detail"
    }
    
    func viewForTableHeader(_ table: BaseTable, size: CGSize) -> UIView? {
        return OrderDetailInfoView(frame: .init(origin: .init(x: 0, y: 0), size: size))
    }
    
    override func viewForHeader(_ table: BaseTable, section: Int, size: CGSize) -> UIView? {
        return nil
    }
    
    override func viewFor(_ table: BaseTable, for section: Int, row: Int, size: CGSize) -> UIView {
        if section == 0 {
            let product = (items as! [Restaurant])[0].menus[0].products[row]
            let color : UIColor = row%2 == 0 ? .white : .posLight
            return OrderDatailProductView(frame: .init(origin: .init(x: 0, y: 0), size: size), backColor: color, product: product)
        } else {
            return OrderDatailPriceInfoView(frame: .init(origin: .init(x: 0, y: 0), size: size))
        }
    }
    
    func heightForRow(_ table:BaseTable , sectoin: Int, row: Int) -> CGFloat {
        return [100,230][sectoin]
    }
    
    
    
    func titleForHeader(_ table: BaseTable, for section: Int) -> String? {
        return ["Product Detail","Price Information"][section]
    }
    
    func heightForHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return 40
    }
    
    func heightForTableHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return 180
    }
    
    override func numberOfRow(_ table: BaseTable, section: Int) -> Int {
        return [(items as! [Restaurant])[0].menus[0].products.count,1][section]
    }
    
    func numberOfSection(_ table: BaseTable) -> Int {
        return 2
    }
}
