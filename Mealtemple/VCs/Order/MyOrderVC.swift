//
//  MyOrderVC.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class MyOrderVC: BaseTableVC {
    
    func heightForHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return 40
    }
    
    override func viewForHeader(_ table: BaseTable, section: Int, size: CGSize) -> UIView? {
        let switchView = SwitchOrderView(frame: .init(x: 0, y: 0, width: size.width, height: size.height),titles: ["Requested","Complited"])
        return switchView
    }
    
    override func viewFor(_ table: BaseTable, for section: Int, row: Int, size: CGSize) -> UIView {
        let product = (items as! [Restaurant])[0].menus[0].products[row]
        let color : UIColor = row%2 == 0 ? .white : .posLight
        return HistoryView(frame: .init(origin: .init(x: 0, y: 0), size: size), backColor: color, product: product)
    }
    
    func selectedRow(_table: BaseTable, section: Int, row: Int) {
        self.nextVC(vc: OrderDetailVC())
    }
    
    func heightForRow(_ table:BaseTable , sectoin: Int, row: Int) -> CGFloat {
        return 150
    }
}
