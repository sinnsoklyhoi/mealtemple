//
//  BaseMapVC.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 12/31/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
import MapKit
import GoogleMaps
class BaseMapVC: BaseTableVC, CLLocationManagerDelegate,GMSMapViewDelegate {
    
    private var locationManager = CLLocationManager()
    private var centerMapCoordinate: CLLocationCoordinate2D!
    private var location : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpMapView()
    }
    
    private func setUpMapView() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        self.locationManager.stopUpdatingLocation()
        print("lat",(location?.coordinate.latitude)!)
        print("lng",(location?.coordinate.longitude)!)
        self.centerMapCoordinate = CLLocationCoordinate2D(latitude: (location?.coordinate.latitude)! , longitude: (location?.coordinate.longitude)!)
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(centerMapCoordinate) { response, error in
            DispatchQueue.main.async {
                let address = response?.firstResult()
                let lines = address?.lines
                self.location = lines?.joined(separator: " , ")
            }
        }
    }
    
    public func getLocation() -> String {
        return self.location ?? ""
    }
}
