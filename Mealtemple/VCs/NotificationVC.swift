//
//  NotificationVC.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    
    static var instances : [String: NotificationVC] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register for events
        let className = String(describing: type(of: self))
        // Remove previous observers
        for eventName in eventsToWatch() {
            if NotificationVC.instances[className] != nil {
                NotificationCenter.default.removeObserver(NotificationVC.instances[className]!, name: NSNotification.Name(rawValue: eventName), object: nil)
            }
        }
        NotificationVC.instances[className] = self
        // Add new observers
        for eventName in eventsToWatch() {
            NotificationCenter.default.addObserver(self, selector: #selector(receivedNotif(_:)), name: NSNotification.Name(rawValue: eventName), object: nil)
        }
    }
    
    func clone() -> NotificationVC {
        return type(of:self).init()
    }
    
    @objc internal func receivedNotif(_ notif:NSNotification) {
        // override if needed
    }
    
    internal func eventsToWatch() -> [String] {
        return []
    }
}
