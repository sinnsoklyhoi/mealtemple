//
//  BaseComponentVC.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class BaseComponentVC: BaseVC , UIScrollViewDelegate {
    
    private var scrollView: UIScrollView!
    private var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let height = self.view.frame.height - self.getHeightNav //- self.getHeightTabBar
        scrollView = UIScrollView(frame: .init(x: 0, y: 0, width: self.view.frame.width, height: height))
        scrollView.isScrollEnabled = false
        scrollView.alwaysBounceVertical = true
        scrollView.bounces  = true
        scrollView.delegate = self
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        scrollView.addSubview(refreshControl)
        self.view.addSubview(scrollView)
        
        for view in setComponent() {
            scrollView.addSubview(view)
        }
        
        if let lastView = scrollView.subviews.last {
            scrollView.contentSize = .init(width: self.view.frame.width, height: lastView.frame.maxY + adjustContentSizeOfScrollView())
            if scrollView.contentSize.height > height {
                scrollView.isScrollEnabled = true
            }
        }
    }
    
    public func setComponent() -> [UIView] {
        return []
    }
    
    internal func adjustContentSizeOfScrollView() -> CGFloat {
        return 0
    }
    
    @objc internal func didPullToRefresh(sender: UIRefreshControl) {
        sender.endRefreshing()
    }
    
    public func getMaxOriginOfView() -> CGFloat {
        if let view = scrollView.subviews.last {
            return view.frame.maxY
        }
        return 0
    }
}

