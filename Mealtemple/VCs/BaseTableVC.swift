//
//  BaseTableVC.swift
//  Mealtemple
//
//  Created by GIS on 12/6/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit

@objc protocol handleClickDelegate {
    @objc optional func onClickCategory(catId: Int)
    @objc optional func onClickMenu(type: String ,id: Int,index: Int)
}

class BaseTableVC: BaseVC , TableViewDelegate , TableViewDataSource , handleClickDelegate {
    
    private var table: BaseTable!
    private var menuView: MenuView!
    private var refreshControl = UIRefreshControl()
    private var menuSelectedIndex: Int = 0
    
    public var items : [DBObject] = []
    
    public var menuIndex: Int {
        set { menuSelectedIndex = newValue }
        get { return menuSelectedIndex }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table = BaseTable(frame: .init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-self.getHeightNav))
        table.tableDelegate = self
        table.tableDatasource = self
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.tintColor = .darkText
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            table.refreshControl = refreshControl
        } else {
            table.addSubview(refreshControl)
        }
        self.view.addSubview(table)
        self.items = self.fetchData()
    }
    
    public func reloadTable() {
        self.table.reloadTable()
    }
    
//    public func fetchData() -> [DBObject] {
//        return setDAO() != nil ? setDAO()!.getCache() : []
//    }
    
    public func fetchData() -> [DBObject] {
        for i in 0...10 {
            let restaurant = Restaurant()
            restaurant.name = "LEE SHOP"
            restaurant.profile = "shoe"
            restaurant.id = i
            restaurant.distance = "121.\(i)"
            restaurant.isActive = i > 5 ? true : false
            restaurant.feeDelivery = 2.3 + Double(i)
            restaurant.cover = "shoe"
            restaurant.rating = Double(2 + i)
            for j in 0...5 {
                let menu = Menu()
                menu.id = j
                menu.name = "A \(j)"
                restaurant.menus.append(menu)
                for k in 0...10 {
                    let product = Product()
                    product.name = "Amazon\(k)"
                    product.id = k
                    product.price = 3.0 + Double(k)
                    product.productImage = "shoe"
                    product.comment = "New product arrive from china."
                    product.rating = Double(1 + k)
                    product.sold = k
                    product.disPrice = k > 4 ? 1.3 : nil
                    restaurant.menus[j].products.append(product)
                }
            }
            items.append(restaurant)
        }
        
        return items
    }
    
    func onClickMenu(type: String, id: Int, index: Int) {
        self.menuSelectedIndex = index
        self.reloadTable()
    }
    
    internal func setDAO() -> BaseDAO? {
        return nil
    }
    
    internal func setMenuItems() -> [DBObject]? {
        return nil
    }
    
    @objc internal func didPullToRefresh(sender: UIRefreshControl) {
        sender.endRefreshing()
    }
}
extension BaseTableVC {
    
    func viewForHeader(_ table:BaseTable,section: Int , size: CGSize) -> UIView? {
        if self.menuSelectedIndex == 0 && menuView == nil {
            let asObject = self.setMenuItems() as? [Menu]
            let menus = asObject != nil ? asObject : nil
            let typeView = MenuView(frame: .init(x: 0, y:0, width: size.width, height: size.height),menus)
            typeView.delegate = self
            self.menuView = typeView
            return typeView
        } else {
            return menuView
        }
    }
    
    func numberOfRow(_ table: BaseTable, section: Int) -> Int {
        return self.items.count
    }
    
    func viewFor(_ table: BaseTable, for section: Int, row: Int, size: CGSize) -> UIView {
        return UIView()
    }
    
    func setCellColor(_ table: BaseTable) -> UIColor {
        return .clear
    }
}
