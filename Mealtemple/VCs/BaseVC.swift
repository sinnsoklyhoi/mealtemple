//
//  BaseVC.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit

class BaseVC: NotificationVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.setTitle()
        self.view.backgroundColor = self.setBackGroundColor()
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = .posGreen
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationController?.setNavigationBarHidden(hideNavigationBar(), animated: false)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
        
        self.setUpBackButton()
        self.setUpBarButton()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setUpBackButton() {
        let controllers = self.navigationController?.viewControllers
        if controllers?.count ?? 0 > 1 {
            let button = UIButton(frame: .init(x: 0, y: 0, width: 25, height: 25))
            button.setImage(UIImage(named: "back")?.withRenderingMode(.alwaysTemplate), for: .normal)
            button.tintColor = .white
            button.contentEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
            button.addTarget(self, action: #selector(goBack), for: .touchUpInside)
            let barButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = barButton
        }
    }
    
    public func nextVC(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    public func popUpVC(vc: UIViewController) {
        vc.modalPresentationStyle = .popover
        let navigation = UINavigationController(rootViewController: vc)
        self.present(navigation, animated: true, completion: nil)
    }
    
    internal func setBackGroundColor() -> UIColor {
        return .white
    }
    
    internal func hideNavigationBar() -> Bool {
        return false
    }
    
    internal func setTitle() -> String {
        return ""
    }
    
    @objc internal func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc internal func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    class HItem {
        
    }
    
    class HNav : HItem {
        var child = [HItem]()
    }
    
    class HVC : HItem {
        var src : NotificationVC
        var pres : HItem? = nil
        
        init(src:NotificationVC) {
            self.src = src
        }
    }
    
    private func setUpBarButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: self.setTitleBarButton(), style: .done, target: self, action: #selector(onClickBarButton(sender:)))
        self.navigationItem.rightBarButtonItem?.tintColor = .posRed
    }
    
    @objc internal func onClickBarButton(sender: UIBarButtonItem) {
        //
    }
    
    internal func setTitleBarButton() -> String {
        return ""
    }
    
    public func changedLanguage() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let tab = appDelegate.window?.rootViewController as! UITabBarController
        let currentTab = tab.selectedViewController!
        let Vcs = getHierarchy(vc: currentTab)
        let vcData = rebuildHierarchy(h:Vcs)
        appDelegate.createTabBarController()
        vcData.todo?()
    }
    
    private func getHierarchy(vc:UIViewController) -> HItem {
        if vc is UINavigationController {
            let hnav = HNav()
            let nav = vc as! UINavigationController
            for child in nav.viewControllers {
                hnav.child.append(getHierarchy(vc: child))
            }
            return hnav
        } else {
            let hvc = HVC(src:vc as! NotificationVC)
            if vc.presentedViewController != nil {
                hvc.pres = getHierarchy(vc:vc.presentedViewController!)
            }
            return hvc
        }
    }
    
    private func rebuildHierarchy(h:HItem) -> (vc:UIViewController, todo:(()->())?) {
        if h is HNav {
            let children = (h as! HNav).child
            let firstInfo = self.rebuildHierarchy(h:children[0])
            let nav = UINavigationController(rootViewController: firstInfo.vc)
            let todo:()->() = {
                firstInfo.todo?()
                for i in 0..<children.count {
                    let childInfo = self.rebuildHierarchy(h:children[i])
                    print("Pushing \(type(of:childInfo.vc)) on \(type(of:firstInfo.vc))")
                    firstInfo.vc.navigationController!.pushViewController(childInfo.vc, animated: false)
                    childInfo.todo?()
                }
            }
            return (vc:nav, todo:todo)
        } else {
            let hvc = h as! HVC
            let clone = hvc.src.clone()
            let todo:(()->()) = {
                if hvc.pres != nil {
                    let presInfo = self.rebuildHierarchy(h: hvc.pres!)
                    print("Presenting \(type(of:presInfo.vc))")
                    clone.navigationController?.present(presInfo.vc, animated: false)
                    presInfo.todo?()
                }
            }
            return (vc:clone, todo:todo)
        }
    }
    
    public func alertController(title: String, message: String, style: UIAlertController.Style,items: [String] = []) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        if style == .alert {
            let no = UIAlertAction(title: Localize.get("NO"), style: .default) { (action) in}
            let yes = UIAlertAction(title: Localize.get("YES"), style: .default) { (action) in
                DispatchQueue.main.async {
                    self.onClickAction(action: action)
                }
            }
            alert.addAction(no)
            alert.addAction(yes)
        } else {
            for item in items {
                let itemAction = UIAlertAction(title: Localize.get(item), style: .default) { (action) in
                    DispatchQueue.main.async {
                        self.onClickAction(action: action)
                    }
                }
                alert.addAction(itemAction)
            }
            let cancel = UIAlertAction(title: Localize.get("CANCEL"), style: .cancel) { (action) in}
            alert.addAction(cancel)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    internal func onClickAction(action: UIAlertAction) {
        //override if need
    }
    
    internal func isClickNo(action: UIAlertAction) {
        //override if need
    }
}
