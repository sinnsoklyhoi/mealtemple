//
//  VerifyCodeVC.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/6/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class VerifyCodeVC: BaseVC {
    
    private var titles: [String] = ["Verification Code","Please enter your code.","your code here"]
    
    override func setTitle() -> String {
        return "Verification Code"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var y: CGFloat = self.getHeightNav + self.getHeightTabBar
        for i in [0,1,2] {
            if i < 2 {
                let label = UILabel(frame: .init(x: 15, y: y, width: self.view.frame.width - 30, height: 30))
                label.text = titles[i]
                if i == 0 {
                    y += 30
                    label.setUpForLargeText()
                } else {
                    y += 40
                    label.setUpForMiddleText()
                }
                self.view.addSubview(label)
            } else {
                let input = UITextField(frame: .init(x: 0, y: y + 10, width: self.view.frame.width - 30, height: 40))
                input.center.x = self.view.center.x
                input.placeholder = titles[i]
                input.textAlignment = .center
                input.textColor = .posRed
                input.layer.borderColor = UIColor.posGray.cgColor
                input.layer.borderWidth = 1
                input.layer.cornerRadius = 5
                input.becomeFirstResponder()
                input.keyboardType = .numberPad
                self.view.addSubview(input)
            }
        }
    }
    
    override func setTitleBarButton() -> String {
        return "Submet"
    }
    
    override func onClickBarButton(sender: UIBarButtonItem) {
        
    }
}
