//
//  ConfirmNumberOrEmailVC.swift
//  Mealtemple
//
//  Created by GIS on 12/18/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class ConfirmNumberOrEmailVC: BaseVC {

    private var titles: [String] = ["Start with your Phone number","We use this to verify your account","Phone Number","Use your Email Address"]
    private var chooseEmail: Bool = false
    
    override func setTitle() -> String {
        return "Login"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    private func setUpView() {
        var y: CGFloat = self.getHeightNav + self.getHeightTabBar
        for i in [0,1,2] {
            let label = UILabel(frame: .init(x: 15, y: y, width: self.view.frame.width - 30, height: 30))
            label.text = titles[i]
            if i == 0 {
                y += 30
                label.setUpForLargeText()
            } else {
                y += 40
                label.setUpForMiddleText()
            }
            self.view.addSubview(label)
        }
        let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
        for _ in [0,1] {
            var textfield = UITextField()
            if !chooseEmail {
                textfield = FPNTextField(frame:.init(x: 15, y: y, width: self.view.frame.width - 30, height: 40))
                (textfield as! FPNTextField).setFlag(countryCode: FPNCountryCode(rawValue: countryCode ?? "KH")!)
                (textfield as! FPNTextField).textAlignment = .left
                (textfield as! FPNTextField).tag = 10
            } else {
                textfield = UITextfieldPadding(frame:.init(x: 15, y: y, width: self.view.frame.width - 30, height: 40))
                (textfield as! UITextfieldPadding).placeholder = "yourname@gmail.com"
                (textfield as! UITextfieldPadding).textAlignment = .left
                (textfield as! UITextfieldPadding).tag = 20
            }
            textfield.layer.borderColor = UIColor.posGray.cgColor
            textfield.layer.borderWidth = 1
            textfield.layer.cornerRadius = 5
            textfield.becomeFirstResponder()
            self.view.addSubview(textfield)
            break
        }
        
        let btn = UIButton()
        btn.frame = .init(x: 15, y: y + 60, width: self.view.frame.width/2 - 30, height: 25)
        btn.setTitle(self.titles[titles.count-1], for: .normal)
        btn.titleLabel?.setUpForSmallText()
        btn.backgroundColor = .posRed
        btn.layer.cornerRadius = 5
        btn.addTarget(self, action: #selector(clickBtn(sender:)), for: .touchUpInside)
        self.view.addSubview(btn)
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    override func onClickBarButton(sender: UIBarButtonItem) {
        if self.view.subviews.filter({$0.tag == 20}).count > 0 {
            let email = (self.view.subviews.filter({$0.tag == 20})[0] as! UITextfieldPadding).text
            if isValidEmail(email ?? "") {
                self.nextVC(vc: VerifyCodeVC())
            } else {
                NotificationTool.showError(message: "Incorrect Email.")
            }
        } else if self.view.subviews.filter({$0.tag == 10}).count > 0{
            let phoneNum = (self.view.subviews.filter({$0.tag == 10})[0] as! FPNTextField).text
            if phoneNum?.count ?? 0 < 8 {
                NotificationTool.showError(message: "Incorrect Phone number.")
            } else {
                self.nextVC(vc: VerifyCodeVC())
            }
        }
    }
    
    override func setTitleBarButton() -> String {
        return "Next"
    }
    
    @objc private func clickBtn(sender: UIButton) {
        for view in view.subviews {
            view.removeFromSuperview()
        }
        
        if chooseEmail {
            self.chooseEmail = false
            self.titles[0] = "Start with your Phone Number"
            self.titles[titles.count-1] = "Use your Email Address"
            self.titles[titles.count-2] = "Phone Number"
        } else {
            self.chooseEmail = true
            self.titles[0] = "Start with your Email Address"
            self.titles[titles.count-1] = "Use your Phone Number"
            self.titles[titles.count-2] = "Email Address"
        }
        self.setUpView()
    }
}
