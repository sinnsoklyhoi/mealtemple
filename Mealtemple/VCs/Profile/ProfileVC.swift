//
//  ProfileVC.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class ProfileVC: BaseTableVC {
    
    private let titles:[String] = ["My Account","Information"]
    private let groups: [[String]] = [["Phone Number","Address","Email","Languages"],["About us","Delivery Policy","Login"]]
    private var userIf: [String] = []
    
    override func fetchData() -> [DBObject] {
        let user = User()
        user.profileUrl = "shoe"
        user.name = "Sinn soklyhoi"
        user.address = "Phnom Penh"
        user.email = "sinnsoklyhoi@gmail.com"
        user.phone = "09876543"
        items.append(user)
        userIf = [user.phone,user.address,user.email,AppManager.getLanguage()]
        return items
    }
    
    func viewForTableHeader(_ table: BaseTable, size: CGSize) -> UIView? {
        let user = items.first as! User
        let profileView  = ProfileView(frame: .init(x: 0, y: 0, width: size.width, height: size.height), user: user)
        return profileView
    }
    
    override func numberOfRow(_ table: BaseTable, section: Int) -> Int {
        return groups[section].count
    }
    
    override func viewFor(_ table: BaseTable, for section: Int, row: Int, size: CGSize) -> UIView {
        let item = self.groups[section][row]
        
        let left = UILabel(frame: .init(origin: .init(x: 20, y: 0), size: size))
        left.text = item
        left.setUpForSmallText()
        
        let right = UILabel(frame: .init(origin: .init(x: left.frame.width/2, y: 0), size: size))
        right.text = section == 0 ?  self.userIf[row] : ""
        right.setUpForSmallText()
        left.addSubview(right)
        
        let line = UIView(frame: .init(x: 10, y: size.height-1.2, width: size.width - 10, height: 0.6))
        line.backgroundColor = .white
        left.addSubview(line)
        
        if section == 1 {
            let icon = UIImageView(frame: .init(x: size.width - 50, y: 10, width: 16, height: size.height/2))
            icon.image = UIImage(named: "next")?.withRenderingMode(.alwaysTemplate)
            icon.tintColor = .posGray
            left.addSubview(icon)
            if row == self.groups[section].count - 1 {
                left.setUpForMiddleText()
            }
        }
        
        //Hello
        
        return left
    }
    
    override func viewForHeader(_ table:BaseTable,section: Int , size: CGSize) -> UIView? {
        let label = UILabel(frame: .init(origin: .init(x: 20, y: 0), size: size))
        label.backgroundColor = .white
        label.text = "   " + titles[section]
        label.setUpForMiddleText()
        return label
    }
    
    func selectedRow(_table: BaseTable, section: Int, row: Int) {
        if section == 1 && row != self.groups[section].count - 1 {
            self.nextVC(vc: row == 0 ? AboutUsVC() : DeliveryPolicyVC())
        } else {
            self.nextVC(vc: ConfirmNumberOrEmailVC())
        }
    }
    
    func heightForTableHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return self.view.frame.width/2 + 100
    }
    
    func heightForRow(_ table:BaseTable , sectoin: Int, row: Int) -> CGFloat {
        return 40
    }
    
    func heightForHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSection(_ table: BaseTable) -> Int {
        return titles.count
    }
    
    override func setCellColor(_ table: BaseTable) -> UIColor {
        return .posLight
    }
    
    func setSelectionStyle() -> UITableViewCell.SeparatorStyle {
        return .singleLine
    }
}
