//
//  ProductDetailVC.swift
//  Mealtemple
//
//  Created by GIS on 12/17/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class ProductDetailVC: BaseTableVC {
    
    private var product: Product!
    
    public func setData(product: Product) {
        self.product = product
    }
    
    override func clone() -> NotificationVC {
        let proDetail = ProductDetailVC()
        proDetail.setData(product: product)
        return proDetail
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let button = UIButton(frame: .init(origin: .init(), size: .init(width: 40, height: 40)))
        button.setTitle(Localize.get("FOOD"), for: .normal)
        button.backgroundColor = .posRed
        button.center = self.view.center
        button.addTarget(self, action: #selector(click), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    @objc func click() {
        UserSession().currentLange = Localize.Language.CHINESE.rawValue
        self.changedLanguage()
    }
    
    override func setTitle() -> String {
        return "Product Detail"
    }
    
    @objc private func addProductToCart() {
        _ = CartDAO().insert(obj: product)
    }
    
    @objc private func orderProduct() {
        if UserSession().isLogined {
            self.nextVC(vc: OrderProductVC(product: product))
        } else {
            self.nextVC(vc: ConfirmNumberOrEmailVC())
        }
    }
}
