//
//  HomeVC.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit

class HomeVC: BaseTableVC {

    override func setTitle() -> String {
        return "Home"
    }
    
    override func setBackGroundColor() -> UIColor {
        return .posLightGray
    }
    
    func viewForTableHeader(_ table: BaseTable, size: CGSize) -> UIView? {
        let catType: [String] = ["GROCERY","FOOD","BEVERAGES","BAKERY","Baby Mart","Flowers","Health","Stationary"]
        let icons: [String] = ["groceries","food","beverages","bakery","babyMart","flower","hospital","stationary"]
        var categories: [Category] = []
        for i in 0..<catType.count {
            let cat = Category()
            cat.name = catType[i]
            cat.icon = icons[i]
            categories.append(cat)
        }
        let view = UIView(frame: .init(x: 0, y: 0, width: size.width, height: size.height))
        view.backgroundColor = .posLightGray
        let headerView = CategoryView(frame: .init(x: 0, y: 0, width:self.view.frame.width, height: size.height), category: categories,location: UserSession().currentLocationString)
        headerView.delegate = self
        headerView.backgroundColor = .white
        view.addSubview(headerView)
        return view
    }
    
    func onClickCategory(catId: Int) {
        self.nextVC(vc: CategoryDetailVC())
    }
    
    override func onClickMenu(type: String, id: Int, index: Int) {
        super.onClickMenu(type: type, id: id, index: index)
        self.alertController(title: "TEST", message: "test", style: .actionSheet,items: ["A1","A2"])
    }
    
    override func viewFor(_ table: BaseTable, for section: Int, row: Int, size: CGSize) -> UIView {
        let restaurant = items[row] as! Restaurant
        let productView = ShopView(frame:.init(x: 0, y: 0, width: size.width, height: size.height),restaurant: restaurant)
        return productView
    }
    
    func heightForTableHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return self.view.frame.width/2 + 100
    }
    
    func heightForRow(_ table:BaseTable , sectoin: Int, row: Int) -> CGFloat {
        return 280
    }
    
    func heightForHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return 40
    }
    
    func selectedRow(_table: BaseTable, section: Int, row: Int) {
        let shopDetail = ShopDetailVC()
        shopDetail.setData(restaurant: items[row] as! Restaurant)
        self.nextVC(vc: shopDetail)
    }
    
    override func setCellColor(_ table: BaseTable) -> UIColor {
        return .posLight
    }
}
