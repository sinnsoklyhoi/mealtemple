//
//  CategoryDetailVC.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class CategoryDetailVC: BaseTableVC {
    
    override func setTitle() -> String {
        return "Category"
    }
    
    func viewForHeader(_table: BaseTable, size: CGSize) -> UIView? {
        let view = UIView(frame: .init(origin: .init(x: 0, y: 0), size: size))
        let proImage = UIImageView(frame: .init(x: 0, y: 0, width: size.width, height: view.frame.height - 40))
        view.addSubview(proImage)
        return view
    }
    
    func heightOfHeader(_table: BaseTable, for section: Int) -> CGFloat {
        return 250
    }
}
