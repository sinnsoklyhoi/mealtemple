//
//  OrderConfiremationVC.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/7/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class OrderConfiremationVC: BaseTableVC {
    
    enum PaymentMethod: String {
        case CASH = "Cash"
        case WALLET = "Wallet"
        case CREDIT_CARD = "Credit Card"
    }
    
    override func setTitle() -> String {
        return "Order Confirmation"
    }
    
    func viewForTableHeader(_ table: BaseTable, size: CGSize) -> UIView? {
        return OrderConfirmView(frame: .init(origin: .init(x: 0, y: 0), size: size))
    }
    
    override func viewFor(_ table: BaseTable, for section: Int, row: Int, size: CGSize) -> UIView {
        let product = (items as! [Restaurant])[0].menus[0].products.first!
        if [0].contains(row) {
            return OrderConfirmProductView(frame: .init(origin: .init(x: 0, y: 0), size: size), backColor: .white, product: product)
        } else if [1].contains(row) {
            return OrderConfirmCouponCodeView(frame: .init(origin: .init(x: 0, y: 0), size: size), backColor: .white, product: product)
        } else if [2].contains(row){
            return OrderConfirmPriceInfoView(frame: .init(origin: .init(x: 0, y: 0), size: size))
        } else if [3].contains(row){
            let method = UIButton(frame: .init(x: 0, y: 0, width: size.width, height: size.height - 10))
            method.setTitle("   CHOOSE PAYMENT METHOD", for: .normal)
            method.setTitleColor(.posRed, for: .normal)
            method.backgroundColor = .white
            method.titleLabel?.setUpForMiddleText()
            method.contentHorizontalAlignment = .left
            method.addTarget(self, action: #selector(chooseMethodPayment), for: .touchUpInside)
            return method
        } else {
            let view = UIView(frame: .init(x: 0, y: 0, width: size.width, height: size.height - 10))
            view.backgroundColor = .white
            
            let icon = UIImageView(frame: .init(x: 5, y: 0, width: view.frame.height, height: view.frame.height))
            icon.image = UIImage(named: "location-pin")?.withRenderingMode(.alwaysTemplate)
            icon.tintColor = .posGray
            icon.contentMode = .center
            view.addSubview(icon)
            
            let location = UILabel(frame: .init(x: icon.frame.width + 10, y: 0, width: size.width-icon.frame.width, height: view.frame.height))
            location.text = UserSession().currentLocationString ?? "Phnom Penh Street #322"
            location.setUpForSmallText()
            view.addSubview(location)
            
            return view
        }
    }
    
    func selectedRow(_table: BaseTable, section: Int, row: Int) {
    }
    
    @objc func chooseMethodPayment(){
        self.alertController(title: "Choose Payment Method", message: "", style: .actionSheet, items: [PaymentMethod.CASH.rawValue,PaymentMethod.WALLET.rawValue,PaymentMethod.CREDIT_CARD.rawValue])
    }
    
    override func setCellColor(_ table: BaseTable) -> UIColor {
        return .posLight
    }
    
    override func numberOfRow(_ table: BaseTable, section: Int) -> Int {
        return 5
    }
    
    func heightForRow(_ table:BaseTable , sectoin: Int, row: Int) -> CGFloat {
        if [3,4].contains(row) {
            return 60
        } else if [2].contains(row) {
            return 180
        }
        return 150
    }
    
    func heightForTableHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return 160
    }

    override func onClickAction(action: UIAlertAction) {
        if let method = PaymentMethod(rawValue: action.title!) {
            if method == .CASH {
                
            } else if method == .WALLET {
                
            } else {
                self.popUpVC(vc: ConfirmCreditCardVC())
            }
        }
    }
}
