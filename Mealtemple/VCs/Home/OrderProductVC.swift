//
//  OrderProductVC.swift
//  Mealtemple
//
//  Created by GIS on 12/18/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import Foundation
class OrderProductVC: BaseTableVC {
    
    private var product: Product!
    
    init(product: Product) {
        super.init(nibName: nil, bundle: nil)
        self.product = product
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setTitle() -> String {
        return "Order"
    }
}
