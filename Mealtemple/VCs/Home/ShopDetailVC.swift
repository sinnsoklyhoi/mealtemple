//
//  ProductDetailVC.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class ShopDetailVC: BaseTableVC {
    
    private var restaurant: Restaurant!
    
    public func setData(restaurant: Restaurant) {
        self.restaurant = restaurant
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func setTitle() -> String {
        return "Shop Detail"
    }

    override func clone() -> NotificationVC {
        let productDetail = ShopDetailVC()
        productDetail.setData(restaurant: restaurant)
        return productDetail
    }
    
    override func setMenuItems() -> [DBObject]? {
        return self.restaurant.menus
    }
    
    func viewForTableHeader(_ table: BaseTable, size: CGSize) -> UIView? {
        let view = UIView(frame: .init(origin: .init(x: 0, y: 0), size: size))
        let cover = UIImageView(frame: .init(x: 0, y: 0, width: size.width, height: view.frame.height))
        cover.image = UIImage(named: restaurant.cover ?? "")
        view.addSubview(cover)
        
        let backPro = UIView(frame: .init(x: 15, y: 20, width: 80, height: 80))
        backPro.backgroundColor = .white
        backPro.layer.cornerRadius = 40
        view.addSubview(backPro)
        
        let profile = UIImageView(frame: .init(x: backPro.frame.width/2-38, y: backPro.frame.height/2-38, width: 76, height: 76))
        profile.clipsToBounds = true
        profile.layer.cornerRadius = 38
        profile.image = UIImage(named:restaurant.profile ?? "")
        backPro.addSubview(profile)
        
        let resName = UILabel(frame: .init(x: 15, y: backPro.frame.maxY + 50, width: view.frame.width/1.5, height: 30))
        resName.numberOfLines = 0
        resName.text = restaurant.name
        resName.setUpForLargeText()
        resName.textColor = .white
        view.addSubview(resName)
        
        let start = StarView(frame: .init(x: 15, y: resName.frame.maxY + 10, width: 100, height: 30))
        start.rating = restaurant.rating ?? 0
        start.settings.disablePanGestures = false
        start.settings.updateOnTouch = false
        start.settings.passTouchesToSuperview = false
        view.addSubview(start)
        
        let feeDelivery = UILabel(frame: .init(x: 15, y: view.frame.maxY-40, width: view.frame.width/1.5, height: 30))
        feeDelivery.text = restaurant.getFeeDelivery()
        feeDelivery.setUpForSmallText()
        feeDelivery.textColor = .white
        view.addSubview(feeDelivery)
        
        return view
    }
    
    func heightForTableHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return 250
    }
    
    override func numberOfRow(_ table: BaseTable, section: Int) -> Int {
        return self.restaurant.menus[menuIndex].products.count
    }
    
    func heightForRow(_ table:BaseTable , sectoin: Int, row: Int) -> CGFloat {
        return 160
    }
    
    func heightForHeader(_ table: BaseTable, for section: Int) -> CGFloat {
        return 40
    }
    
    override func viewFor(_ table: BaseTable, for section: Int, row: Int, size: CGSize) -> UIView {
        let product = self.restaurant.menus[menuIndex].products[row]
        let color : UIColor = row%2 == 0 ? .white : .posLight
        let productView = ShopDetailView(frame: .init(origin: .init(x: 0, y: 0), size: size),backColor: color, product: product)
        productView.tag = row
        return productView
    }
    
    func selectedRow(_table: BaseTable, section: Int, row: Int) {
        self.nextVC(vc: OrderConfiremationVC())
    }
}
