//
//  VCExtension.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
extension UIViewController {
    public class func topVC(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topVC(controller: navigationController.visibleViewController)
        } else if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topVC(controller: selected)
            }
        } else if let presented = controller?.presentedViewController {
            return topVC(controller: presented)
        }
        return controller
    }
    
    public var getHeightNav : CGFloat {
        return /*UIApplication.shared.statusBarFrame.size.height +*/ (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    public var getHeightTabBar : CGFloat {
        return self.tabBarController?.tabBar.frame.height ?? 0.0
    }
}
