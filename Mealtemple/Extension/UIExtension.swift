//
//  UIExtension.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init(netHex:Int, opacity:CGFloat = 1.0) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff, opacity: opacity)
    }
    public convenience init(red: Int, green: Int, blue: Int, opacity: CGFloat = 1.0) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: opacity)
    }
    
    public static let posRed = UIColor(netHex: 0xEE606F)
    public static let posGray = UIColor(netHex: 0x646464)
    public static let posGreen = UIColor(netHex: 0x0F7464)
    public static let posLightGray = UIColor(netHex: 0xEAEAEA)
    public static let posLight = UIColor(netHex: 0xF2F4F6)
}

extension UILabel {
    private func setUpLabel() {
        self.font = .systemFont(ofSize: 13)
        self.textColor = .lightGray
    }
    
    public func setUpForLargeText(_ fontSize: Int = 17) {
        self.textColor = .posGray
        self.font = .boldSystemFont(ofSize: CGFloat(fontSize))
    }
    
    public func setUpForSmallText() {
        self.setUpLabel()
    }
    
    public func setUpForMiddleText() {
        self.setUpLabel()
        self.font = .systemFont(ofSize: 18)
    }
}

extension UIView {
    public func getMaxYInView(view: UIView) -> CGFloat {
        print(view.subviews.count)
        return view.subviews.last?.frame.maxY ?? 0
    }
}

extension UIButton {
    public func setUpPosRedButton() {
        self.backgroundColor = .posRed
        self.tintColor = .white
        self.layer.cornerRadius = 3
    }
}

extension String {
    
    public func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    public func strikeThrough(font: CGFloat = 14) -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttributes([
                                        NSAttributedString.Key.font : UIFont.systemFont(ofSize: font),
                                        NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue,
                                    ],range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}

extension RangeReplaceableCollection where Element: Equatable {
    @discardableResult
    public mutating func removeFirstEqual(_ element: Element) -> Element? {
        guard let index = firstIndex(of: element) else { return nil }
        return remove(at: index)
    }
}
