//
//  BaseTextfieldView.swift
//  Mealtemple
//
//  Created by GIS on 12/11/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class BaseTextfieldView: BaseView {
    
    override func setComponent() -> UIView? {
        
        let view = UIView(frame: .init(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        self.setActionOnView(view: view)
        let logo = UIImageView(frame: .init(x:view.frame.width * 0.25, y: 20, width: self.frame.width/2, height: 60))
        logo.image = UIImage(named: "logo")?.withRenderingMode(.alwaysTemplate)
        logo.tintColor = .posGreen
        view.addSubview(logo)
        
        var y : CGFloat = logo.frame.maxY + 30
        for i in 0...self.placeholder().count - 1 {
            let field = UITextfieldPadding(frame: CGRect(x: 15, y: y, width: self.frame.width - 30, height: self.fieldHeight()))
            field.backgroundColor = .clear
            field.textColor = self.setTextColor()
            field.tintColor = .posRed
            field.layer.borderColor = self.borderColor().cgColor
            field.layer.borderWidth = 1
            field.layer.cornerRadius = self.setCornerRadius()
            field.font = .systemFont(ofSize: 14)
            field.tag = i
            field.attributedPlaceholder = NSAttributedString(string: self.placeholder()[i] ,attributes: [NSAttributedString.Key.foregroundColor: UIColor.posGray])
            field.rightViewMode = .always
            field.addTarget(self, action: #selector(textChange(field:)), for: .editingChanged)
            view.addSubview(field)
            
            if isSecureTextEntry() == self.placeholder()[i] {
                field.isSecureTextEntry = true
            }
            
            for f in setFieldToShowNumberPad() {
                if f == placeholder()[i] {
                    field.keyboardType = .numberPad
                }
            }
            
            y += self.fieldHeight() + self.setSpaceBetweenTextfield()
        }
        
        let btn = UIButton(frame: .init(x: 15, y: y + self.fieldHeight() + 30, width: self.frame.width - 30, height: 40))
        btn.backgroundColor = .posGreen
        btn.setTitle(self.setTitleBtn(), for: .normal)
        btn.layer.cornerRadius = 10
        btn.titleLabel?.textColor = .white
        btn.addTarget(self, action: #selector(onClickBtn(sender:)), for: .touchUpInside)
        view.addSubview(btn)
        
        view.frame.size.height = btn.frame.maxY
        
        if self.insertView(from: btn.frame) != nil {
            view.addSubview(insertView(from: btn.frame)!)
            view.frame.size.height = insertView(from: btn.frame)!.frame.maxY
        }
        
        return view
    }
    
    override func getMaxYInView() -> CGFloat {
        return self.setComponent()?.frame.height ?? 0
    }
    
    override func onClickView(sender: UITapGestureRecognizer) {
        UIViewController.topVC()?.view.endEditing(true)
    }
    
    @objc internal func textChange(field : UITextField) {
        //ovverride
    }
    
    @objc internal func onClickBtn(sender: UIButton) {
        //ovverride
    }

    internal func fieldHeight() -> CGFloat {
        return 40
    }
    
    internal func setCornerRadius() -> CGFloat {
        return 10
    }
    
    internal func setFieldToShowNumberPad() -> [String] {
        return []
    }
    
    internal func borderColor() -> UIColor {
        return UIColor.posGreen
    }
    
    internal func setTextColor() -> UIColor {
        return .darkText
    }
    
    internal func placeholder() -> [String] {
        return []
    }
    
    internal func setTitleBtn() -> String {
        return ""
    }
    
    internal func setSpaceBetweenTextfield() -> CGFloat {
        return 5
    }
    
    internal func insertView(from view: CGRect) -> UIView? {
        return nil
    }
    
    internal func isSecureTextEntry() -> String? {
        return nil
    }
}
