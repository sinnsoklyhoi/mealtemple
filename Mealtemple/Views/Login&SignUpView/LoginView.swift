//
//  LoginView.swift
//  Mealtemple
//
//  Created by GIS on 12/11/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class LoginView: BaseTextfieldView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.frame.size.height = self.getMaxYInView()
    }
    
    override func placeholder() -> [String] {
        return ["User name","Phone number","Email", "Address"]
    }
    
    override func setFieldToShowNumberPad() -> [String] {
        return ["Phone number"]
    }
    
    override func setTitleBtn() -> String {
        return "Login"
    }
    
    override func insertView(from view: CGRect) -> UIView? {
       // add extra view if need
        return nil
    }
    
    override func onClickBtn(sender: UIButton) {
        //handle click button
    }
    
    override func textChange(field: UITextField) {
        //when textchange in textfield
    }
}
