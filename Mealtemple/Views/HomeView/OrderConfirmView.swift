//
//  OrderConfirmView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/7/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class OrderConfirmView: OrderDetailInfoView {
    
    override func setTitleInfo() -> [String] {
        return ["Receiver Name","Receiver Phone","House Number","Location"]
    }
    
    override func setValueInfo() -> [String] {
        return ["lyhor","098765432","3323","Phnom Penh"]
    }
    
    override func customInitView(view: inout UIView) {
        for item in view.subviews {
            if [1].contains(item.tag) {
                (item as! UILabel).text = "SHIPPING INFORMATION"
                let editBtn = UIButton(frame: .init(x: view.frame.width - 45, y: 15, width: 30, height: 30))
                editBtn.setImage(UIImage(named:"edit")?.withRenderingMode(.alwaysTemplate), for: .normal)
                editBtn.tintColor = .posRed
                view.addSubview(editBtn)
            }
        }
    }
}
