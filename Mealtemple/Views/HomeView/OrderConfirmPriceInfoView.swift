//
//  OrderConfirmPriceInfoView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/7/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class OrderConfirmPriceInfoView: OrderDetailInfoView {
    
    override func setTitleInfo() -> [String] {
        return ["Total Qty","Sub total","VAT(10%)","Delivery Fee","Total"]
    }
    
    override func setValueInfo() -> [String] {
        return ["1","$2.00","$0.35","$1","$3.35"]
    }
    
    override func customInitView(view: inout UIView) {
        view.backgroundColor = .white
        view.frame.size.height = view.frame.height - 10
        view.frame.origin.y = 0
        for item in view.subviews {
            item.frame.origin.y -= 15
            if [1].contains(item.tag) {
                (item as! UILabel).text = "SUMMARY"
            } else if item.frame.maxY > view.frame.height - 30 {
                (item as! UILabel).setUpForLargeText()
                (item as! UILabel).textColor = .posRed
            }
        }
    }
}
