//
//  ChangeTypeView.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class MenuView: BaseMenu {

    private var customeMenu: [Menu] = []
    
    init(frame: CGRect,_ customeMenu: [Menu]?) {
        self.customeMenu = customeMenu ?? []
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setTitleMenu() -> [String] {
        var items: [String] = []
        
        for type in Product().allProductType() {
            items.append(type.rawValue)
        }
        let menuNames = customeMenu.map({$0.name!})
        let menuTitles = customeMenu.count > 0 ? menuNames : items
        return menuTitles
    }
    
    @objc override func switchingControl(sender: BetterSegmentedControl) {
        self.delegate.onClickMenu?(type: sender.getTitle(index: sender.index),id:0, index: sender.index)
    }
}
