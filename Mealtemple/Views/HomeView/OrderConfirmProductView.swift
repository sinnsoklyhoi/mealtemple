//
//  OrderConfirmProductView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/7/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class OrderConfirmProductView: OrderDatailProductView {
    
    override func customInitView(view: inout UIView) {
        super.customInitView(view: &view)
        view.frame.size.height = view.frame.height - 10
        view.frame.origin.y = 10
        for item in view.subviews {
            if [1].contains(item.tag) {
                let resName = UILabel(frame:.init(x: 15, y: 0, width: view.frame.width*0.4, height: 30))
                resName.setUpForLargeText()
                resName.text = "LEE SHOP"
                view.addSubview(resName)
                item.frame = .init(x:item.frame.origin.x,y:resName.frame.maxY + 5,width: view.frame.height - 30, height: view.frame.height - 45)
            } else if [2,3].contains(item.tag) {
                item.frame.origin = .init(x: item.frame.origin.x - 15, y: item.frame.origin.y + 15)
            } else if [7].contains(item.tag) {
                item.removeFromSuperview()
            }
        }
        var x: CGFloat = view.frame.width - 120
        for i in 8...10 {
            let btn = UIButton(frame: .init(x: x, y: view.frame.height/2 - 15, width: 30, height: 30))
            btn.tag = i
            btn.setTitle("\(getProduct().quality)", for: .normal)
            if [8,10].contains(i) {
                let title = i == 8 ? "-" : "+"
                btn.setTitle(title, for: .normal)
                btn.layer.cornerRadius = 15
                btn.backgroundColor = .posLight
                btn.addTarget(self, action:#selector(updateUserActivityState(_:)) , for: .touchUpInside)
            }
            btn.setTitleColor(.posGray, for: .normal)
            view.addSubview(btn)
            x += 30
        }
    }
    
    private func updateQualityProduct(sender: UIButton) {
        
    }
}
