//
//  ProductDetailView.swift
//  Mealtemple
//
//  Created by GIS on 12/9/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit

protocol UpdateQualityDelegate {
    func addQuality(index: Int)
    func subQuality(index: Int)
}

class ShopDetailView: BaseView {
    
    private var product: Product!
    private var backColor: UIColor!
    public var qualityDeleget: UpdateQualityDelegate!
    
    init(frame: CGRect,backColor: UIColor, product: Product) {
        self.product = product
        self.backColor = backColor
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setComponent() -> UIView? {
        
        let view = UIView(frame: .init(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        view.backgroundColor = self.backColor
        
        let proImage = UIImageView(frame: .init(x: 10, y: 20, width: view.frame.height - 40, height: view.frame.height - 40))
        proImage.clipsToBounds = true
        proImage.layer.cornerRadius = 10
        proImage.tag = 1
        proImage.image = UIImage(named: product.productImage)
        view.addSubview(proImage)
        
        let proName = UILabel(frame: .init(x: proImage.frame.maxX + 10, y: 20, width: view.frame.width/1.5, height: 20))
        proName.numberOfLines = 0
        proName.tag = 2
        proName.text = product.name
        proName.setUpForMiddleText()
        view.addSubview(proName)
        
        let combination = NSMutableAttributedString()
        let oldPrice = product.formatToString(price: product.price).strikeThrough()
        let afterDis = NSAttributedString(string: product.getSalePrice())
        
        if product.disPrice != nil {
            combination.append(oldPrice)
            combination.append(NSAttributedString(string: " To "))
            combination.append(afterDis)
        } else {
            combination.append(afterDis)
        }
        
        let proPrice = UILabel(frame: .init(x: proImage.frame.maxX + 10, y: proName.frame.maxY + 10, width: view.frame.width/1.5, height: 20))
        proPrice.numberOfLines = 0
        proPrice.tag = 3
        proPrice.setUpForLargeText()
        proPrice.attributedText = combination
        view.addSubview(proPrice)
        
        
        let start = StarView(frame: .init(x: proImage.frame.maxX + 10, y: proPrice.frame.maxY + 10, width: 50, height: 15))
        start.settings.starSize = 13
        start.rating = product.rating ?? 0
        start.tag = 4
        start.settings.disablePanGestures = false
        start.settings.updateOnTouch = false
        start.settings.passTouchesToSuperview = false
        view.addSubview(start)
        
        let sold = UILabel(frame: .init(x: start.frame.maxX + 5, y: proPrice.frame.maxY + 10, width: 80, height: 15))
        sold.text  = ".Sold \(product.sold ?? 0)"
        sold.setUpForSmallText()
        sold.tag = 5
        view.addSubview(sold)
        
        let detail = UILabel(frame: .init(x: proImage.frame.maxX + 10, y: proImage.frame.height-5, width: view.frame.width - proImage.frame.width - 20, height: 20))
        detail.numberOfLines = 0
        detail.text = product.comment ?? ""
        detail.tag = 6
        detail.setUpForSmallText()
        view.addSubview(detail)
        
        return view
    }
}
