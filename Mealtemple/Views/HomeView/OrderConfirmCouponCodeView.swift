//
//  OrderConfirmCouponCodeView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/7/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class OrderConfirmCouponCodeView: OrderConfirmProductView {
    
    override func customInitView(view: inout UIView) {
        super.customInitView(view: &view)
        view.frame.size.height = view.frame.height - 10
        view.frame.origin.y = 10
        
        var x: CGFloat = 0
        for item in view.subviews {
            if [8,9,10].contains(item.tag) {
                let label = UILabel(frame: .init(x: view.frame.width - 120, y: 30, width: 100, height: 30))
                label.text = "Rider Tip($)"
                label.setUpForSmallText()
                label.sizeToFit()
                if [8].contains(item.tag) {
                    view.addSubview(label)
                    let inputCoupon = UITextfieldPadding(frame: .init(x: 15, y: label.frame.maxY + 20, width: view.frame.width - 130, height: 40))
                    inputCoupon.placeholder = "Coupon Code"
                    inputCoupon.layer.cornerRadius = 5
                    inputCoupon.layer.borderColor = UIColor.posGray.cgColor
                    inputCoupon.layer.borderWidth = 0.5
                    view.addSubview(inputCoupon)
                    
                    let applyBtn = UIButton(frame: .init(x: inputCoupon.frame.maxX + 20, y: label.frame.maxY + 20, width: 70, height: 40))
                    applyBtn.setTitle("APPLY", for: .normal)
                    applyBtn.setTitleColor(.white, for: .normal)
                    applyBtn.backgroundColor = .posRed
                    applyBtn.layer.cornerRadius = 5
                    view.addSubview(applyBtn)
                }
                label.frame.origin.x = view.frame.width - 130 - label.frame.width
                item.frame.origin = .init(x:label.frame.maxX + 10 + x, y: 20)
                x += 30
            } else {
                item.removeFromSuperview()
            }
        }
    }
}
