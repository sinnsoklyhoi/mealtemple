//
//  ProductView.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class ShopView: BaseView {
    
    public var restaurant: Restaurant!
    private var maxY: CGFloat = 0
    
    init(frame: CGRect, restaurant: Restaurant) {
        self.restaurant = restaurant
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setComponent() -> UIView? {
        
        let view = UIView(frame: .init(x: 5, y: 5, width: self.frame.width - 10, height: self.frame.height - 10))
        view.backgroundColor = .white
        view.layer.cornerRadius = 5

        let proImage = UIImageView(frame: .init(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 100))
        proImage.image = UIImage(named: restaurant.profile ?? "")
        proImage.clipsToBounds = true
        proImage.layer.cornerRadius = 5
        view.addSubview(proImage)

        let backPro = UIView(frame: .init(x: proImage.frame.maxX - 95, y: proImage.frame.maxY - 40, width: 80, height: 80))
        backPro.backgroundColor = .white
        backPro.layer.cornerRadius = 40
        view.addSubview(backPro)
        
        let profile = UIImageView(frame: .init(x: backPro.frame.width/2-35, y: backPro.frame.height/2-35, width: 70, height: 70))
        profile.clipsToBounds = true
        profile.layer.cornerRadius = 35
        profile.image = UIImage(named:restaurant.profile ?? "")
        backPro.addSubview(profile)
        
        let backActive = UIView(frame: .init(x: profile.frame.width-26, y: profile.frame.height-20, width: 26, height: 26))
        backActive.backgroundColor = .white
        backActive.layer.cornerRadius = 13
        backPro.addSubview(backActive)
        
        let activeView = UIView(frame: .init(x: backActive.frame.width/2-9, y: backActive.frame.height/2-9, width: 18, height: 18))
        activeView.layer.cornerRadius = 9
        activeView.backgroundColor = restaurant.isActive == true ? .posGreen : .posRed
        backActive.addSubview(activeView)
        
        let feeDelivery = UILabel(frame: .init(x: view.frame.width/2 - 10, y: proImage.frame.maxY + 50, width: view.frame.width/2, height: 20))
        feeDelivery.text = restaurant.getFeeDelivery()
        feeDelivery.setUpForSmallText()
        feeDelivery.textAlignment = .right
        feeDelivery.textColor = .posGray
        view.addSubview(feeDelivery)
        
        let proName = UILabel(frame: .init(x: 10, y: proImage.frame.maxY + 20, width: view.frame.width - 20, height: 20))
        proName.setUpForLargeText()
        proName.text = restaurant.name
        view.addSubview(proName)
        
        let start = StarView(frame: .init(x: 10, y: proName.frame.maxY + 5, width: 50, height: 15))
        start.settings.starSize = 13
        start.rating = restaurant.rating ?? 0
        start.settings.disablePanGestures = false
        start.settings.updateOnTouch = false
        start.settings.passTouchesToSuperview = false
        view.addSubview(start)
        
        let mapView = UIImageView(frame: .init(x: 10, y: start.frame.maxY + 6, width: 16, height: 16))
        mapView.tintColor = .lightGray
        mapView.contentMode = .scaleAspectFit
        mapView.image = UIImage(named:"location-pin")?.withRenderingMode(.alwaysTemplate)
        view.addSubview(mapView)
        
        let distance = UILabel(frame: .init(x: mapView.frame.maxX + 3, y: start.frame.maxY + 5, width: view.frame.width - 20, height: 24))
        distance.setUpForSmallText()
        distance.text = restaurant.getDistanceFormat()
        view.addSubview(distance)
        return view
    }
}
