//
//  CategoryView.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class CategoryView: BaseView {
    
    private var categories: [Category] = []
    private var location: String?
    
    init(frame: CGRect, category: [Category],location: String?) {
        self.categories = category
        self.location = location
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setComponent() -> UIView? {
        
        var x: CGFloat = 0
        var y: CGFloat = 90
        var index: Int = 0
        let backView = UIView(frame: .init(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        
        let location = UILabel(frame: .init(x: 10, y: 5, width: self.frame.width - 20, height: 40))
        location.setUpForMiddleText()
        location.text = self.location ?? "TEST"
        location.textAlignment = .center
        location.layer.borderWidth = 0.5
        location.layer.cornerRadius = 5
        backView.addSubview(location)
        
        let headerTitle = UILabel(frame: .init(x: 0, y: 55, width: self.frame.width, height: 30))
        headerTitle.setUpForLargeText()
        headerTitle.text = "Popular Categories"
        headerTitle.textAlignment = .center
        backView.addSubview(headerTitle)
        
        for cat in categories {
            let view = UIView(frame: .init(x: x, y: y, width: self.frame.width/4, height:self.frame.width/4))
            view.tag = index
            backView.addSubview(view)
            let icon = UIImageView(frame: .init(x: view.frame.width/2 - view.frame.width*0.3/2, y:20, width: view.frame.width*0.3, height:view.frame.height*0.3))
            self.setActionOnView(view: view)
            icon.image = UIImage(named: cat.icon)
            icon.contentMode = .scaleAspectFit
            view.addSubview(icon)
            
            let catName = UILabel(frame: .init(x: 0, y: icon.frame.maxY + 10, width: view.frame.width, height: 20))
            catName.text = cat.asString()
            catName.textAlignment = .center
            catName.setUpForSmallText()
            view.addSubview(catName)
            
            x += self.frame.width/4
            if x == self.frame.width {
                x = 0
                y += self.frame.width/4
            }
            index += 1
        }
        return backView
    }
    
    override func onClickView(sender: UITapGestureRecognizer) {
        self.delegate.onClickCategory?(catId: sender.view!.tag)
    }
    
    override func getMaxYInView() -> CGFloat {
        return setComponent()?.frame.height ?? 0
    }
}
