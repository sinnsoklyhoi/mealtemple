//
//  ProfileDetail.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/2/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class ProfileDetail: BaseView {
    
    private let groups: [String:[String]] = ["My Account":["Phone Number","Address","Email","Languages"],
                                             "Information":["About us","Delivery Policy"]]
    
    override func setComponent() -> UIView? {
        let view = UIView(frame: .init(x: 0, y: 50, width: self.frame.width, height: self.frame.height))
        
        var y:CGFloat = 0
        
        for (key,items) in groups {
            
            let backView = UIView(frame: .init(x: 0, y: y, width: self.frame.width, height: CGFloat((items.count+1) * 30)))
            backView.backgroundColor = .posLightGray
            view.addSubview(backView)
            
            let title = UILabel(frame: .init(x: 15, y: backView.frame.minY + 10, width: view.frame.width*0.8, height: 30))
            title.text = key
            title.setUpForLargeText()
            backView.addSubview(title)
            y += 40
            for item in items {
                let itemTxt = UILabel(frame: .init(x: 20, y: y, width: view.frame.width*0.8, height: 30))
                itemTxt.text = item
                itemTxt.setUpForMiddleText()
                backView.addSubview(itemTxt)
                y += 40
            }
        }
        return view
    }
}
