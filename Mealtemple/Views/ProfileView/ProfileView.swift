//
//  ProfileView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/2/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class ProfileView: BaseView {
    
    private var user: User!
    
    init(frame: CGRect,user: User) {
        self.user = user
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setComponent() -> UIView? {
        let view = UIView(frame: .init(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 50))

        let profile = UIImageView(frame: view.frame)
        profile.image = UIImage(named:user.profileUrl)
        view.addSubview(profile)
        
        let name = UILabel(frame:.init(x:10, y: profile.frame.maxY - 45, width: view.frame.width - 20, height:35))
        name.text = user.name
        name.setUpForLargeText()
        name.textColor = .white
        view.addSubview(name)
        
        return view
    }
}
