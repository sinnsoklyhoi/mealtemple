//
//  BaseMenu.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/3/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class BaseMenu: BaseView {
        
    private var segment: BetterSegmentedControl!
    
    override func setComponent() -> UIView? {
        let scroll = UIScrollView(frame: .init(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        scroll.showsVerticalScrollIndicator = false
        scroll.showsHorizontalScrollIndicator = false
        let count = setTitleMenu().count < 4 ? 2 : 4
        scroll.contentSize.width = self.frame.width/CGFloat(count) * CGFloat(setTitleMenu().count)
        
        segment = .appleStyled(frame: CGRect(x: 0,y:0,width:scroll.contentSize.width, height: 40),titles:setTitleMenu())
        segment.addTarget(self, action: #selector(switchingControl(sender:)), for: .valueChanged)
        scroll.addSubview(segment)
        return scroll
    }
    
    internal func setTitleMenu() -> [String]{
        return []
    }
    
    @objc internal func switchingControl(sender: BetterSegmentedControl) {
        //
    }
}

