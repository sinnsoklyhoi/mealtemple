//
//  BaseTableView.swift
//  Mealtemple
//
//  Created by GIS on 12/6/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit

@objc protocol TableViewDelegate {
    @objc optional func selectedRow(_table: BaseTable, section: Int , row: Int)
}

@objc protocol TableViewDataSource {
    @objc optional func heightForRow(_ table:BaseTable , sectoin: Int, row: Int) -> CGFloat
    @objc func numberOfRow(_ table:BaseTable, section: Int) -> Int
    @objc optional func titleForHeader(_ table:BaseTable,for section: Int) -> String?
    @objc optional func viewForTableHeader(_ table:BaseTable, size: CGSize) -> UIView?
    @objc optional func viewForHeader(_ table:BaseTable,section: Int , size: CGSize) -> UIView?
    @objc optional func headerAlign(_ table:BaseTable, section: Int) -> NSTextAlignment
    @objc optional func valueFor(_ table:BaseTable,for section:Int, row: Int) -> String
    @objc func viewFor(_ table:BaseTable,for section:Int, row: Int, size:CGSize) -> UIView
    @objc optional func heightForTableHeader(_ table:BaseTable,for section:Int) -> CGFloat
    @objc optional func heightForHeader(_ table:BaseTable,for section:Int) -> CGFloat
    @objc optional func numberOfSection(_ table: BaseTable) -> Int
    @objc optional func scrollViewDidScroll(_ scrollView: UIScrollView)
    @objc optional func setCellColor(_ table: BaseTable) -> UIColor
    @objc optional func setSeparatorStyle() -> UITableViewCell.SeparatorStyle
}

class BaseTable: UIView,UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate{
    
    private let tableview = UITableView()
    private var headerViews: UIView!
    private let identifire = "UITableViewCell"
    
    public var tableDelegate : TableViewDelegate!
    public var tableDatasource : TableViewDataSource!
    
    public var isScrollEnabled : Bool {
        get {
            return self.tableview.isScrollEnabled
        }
        set {
            self.tableview.isScrollEnabled = newValue
        }
    }
    
    public var refreshControl : UIRefreshControl {
        get {
            return tableview.refreshControl!
        } set {
            tableview.refreshControl = newValue
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupObjects()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupObjects()
    }
    
    private func setupObjects() {
        
        self.layer.cornerRadius = 5
        tableview.separatorColor = .posLightGray
        tableview.layoutMargins = UIEdgeInsets.zero
        tableview.separatorInset = UIEdgeInsets.zero
        tableview.delegate = self
        tableview.dataSource = self
        tableview.showsVerticalScrollIndicator = false
        tableview.showsHorizontalScrollIndicator = false
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: identifire)
        self.addSubview(tableview)
    }
    
    
    override func layoutSubviews() {
        tableview.frame = self.frame
        if self.tableDatasource != nil {
            let size = CGSize(width: self.frame.width, height: self.tableDatasource.heightForTableHeader?(self, for: 0) ?? 0)
            self.tableview.tableHeaderView = self.tableDatasource.viewForTableHeader?(self,size: size)
            self.tableview.separatorStyle = self.tableDatasource.setSeparatorStyle?() ?? .none
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.tableDatasource.scrollViewDidScroll?(scrollView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableDatasource.numberOfRow(self, section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableDatasource.heightForRow?(self, sectoin: indexPath.section, row: indexPath.row) ?? 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifire, for: indexPath)
        for view in cell.subviews {
            view.removeFromSuperview()
        }
        cell.backgroundColor = self.tableDatasource.setCellColor?(self)
        let view = self.tableDatasource.viewFor(self, for: indexPath.section, row: indexPath.row, size: cell.frame.size)
        cell.selectionStyle = .none
        cell.indentationLevel = 2;
        cell.addSubview(view)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableDelegate.selectedRow?(_table: self, section: indexPath.section, row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = self.tableDatasource.viewForHeader?(self, section: section, size: .init(width: self.frame.width, height: self.tableDatasource.heightForHeader?(self, for: section) ?? 0))
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.tableDatasource.titleForHeader?(self, for: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.tableDatasource.heightForHeader?(self, for: section) ?? self.tableDatasource.heightForHeader?(self, for: section) ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableDatasource.numberOfSection?(self) ?? 1
    }
    
    public func reloadTable() {
        self.tableview.reloadData()
    }
}
