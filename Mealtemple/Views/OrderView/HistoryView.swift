//
//  HistoryView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/3/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class HistoryView: ShopDetailView {
    
    private var product: Product!
    private var backColor: UIColor!
    
    override init(frame: CGRect, backColor: UIColor, product: Product) {
        self.product = product
        self.backColor = backColor
        super.init(frame: frame, backColor: backColor, product: product)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func customInitView(view: inout UIView) {
        var getView = UIView()
        for item in view.subviews {
            if [4,5,6].contains(item.tag) {
                item.removeFromSuperview()
            }
            
            if [3].contains(item.tag) {
                (item as! UILabel).attributedText = nil
                (item as! UILabel).text = product.getSalePrice()
            }
            
            if [1].contains(item.tag) {
                getView = item
            }
        }
        let date = UILabel(frame: .init(x:getView.frame.maxX + 10, y: getView.frame.maxY - 20, width: view.frame.width/2, height: 20))
        date.text = "12/30/2019 02:30 PM"
        date.tag = 7
        date.setUpForSmallText()
        view.addSubview(date)
    }
    
    public func getProduct() -> Product {
        return self.product
    }
}
