//
//  OrderDatailPriceInfoView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/6/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class OrderDatailPriceInfoView: OrderDetailInfoView {
    
    override func setTitleInfo() -> [String] {
        return ["Subtotal","Delivery fee","Discount","Rider tip","Vat","Total Price"]
    }
    
    override func setValueInfo() -> [String] {
        return ["$10.00","$5.00","$0.00","$0.00","$1.00","$16.00"]
    }
    
    override func customInitView(view: inout UIView) {
        for item in view.subviews {
            if [1].contains(item.tag) {
                (item as! UILabel).text = "Price Summary"
            }
        }
    }
}
