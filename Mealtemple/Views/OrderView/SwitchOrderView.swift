//
//  SwitchOrderView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/3/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class SwitchOrderView: BaseMenu {
    
    private var titles: [String] = []
       
       init(frame: CGRect,titles: [String]?) {
           self.titles = titles ?? []
           super.init(frame: frame)
       }
       
       required init?(coder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }
    
    override func setTitleMenu() -> [String] {
        return self.titles
    }
}
