//
//  OrderDatailProductView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/6/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class OrderDatailProductView: HistoryView {
    override func customInitView(view: inout UIView) {
        super.customInitView(view: &view)
        for item in view.subviews {
            if [7].contains(item.tag) {
                item.frame.origin = .init(x: view.frame.width/2 - 10, y: view.frame.height/2 - 15)
                (item as! UILabel).textAlignment = .right
                (item as! UILabel).text = "\(self.getProduct().quality)"
            } else if [1].contains(item.tag) {
                item.frame.size = .init(width: view.frame.height - 20, height: view.frame.height - 20)
                item.frame.origin = .init(x: item.frame.origin.x, y: 10)
            } else if [2,3].contains(item.tag) {
                item.frame.origin = .init(x: view.frame.height + 10, y: item.frame.origin.y)
            }
        }
    }
}
