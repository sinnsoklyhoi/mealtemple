//
//  OrderDetailInfoView.swift
//  Mealtemple
//
//  Created by Sinn soklyhoi on 1/6/20.
//  Copyright © 2020 GIS. All rights reserved.
//

import Foundation
class OrderDetailInfoView: BaseView {
    
    override func setComponent() -> UIView? {
        let view = UIView(frame:self.frame)
        let resName = UILabel(frame: .init(x: 15, y: 15, width: self.frame.width*0.7, height: 30))
        resName.text = "LEE SHOP"
        resName.setUpForLargeText()
        resName.tag = 1
        view.addSubview(resName)
        
        var y: CGFloat = resName.frame.maxY + 10
        var i: Int = 0
        for info in self.setTitleInfo() {
            let label = UILabel(frame: .init(x: 15, y: y, width: self.frame.width/2.5, height: 20))
            label.setUpForSmallText()
            label.text = "\(info) :"
            label.textAlignment = .left
            view.addSubview(label)
            
            let value = UILabel(frame: .init(x: self.frame.width/2.5, y: y, width: self.frame.width/1.5, height: 20))
            value.setUpForSmallText()
            value.text = setValueInfo()[i]
            value.textAlignment = .left
            view.addSubview(value)
            i += 1
            y += 25
        }
        return view
    }
    
    public func setTitleInfo() -> [String] {
        return ["Tel","Open Hours","Order Number","Order Time","Delivery Time"]
    }
    
    public func setValueInfo() -> [String] {
        return ["09876543","9:00 AM - 10:00 PM","000007","12/31/2019 02:10 PM","12/31/2019 02:10 PM"]
    }
}
