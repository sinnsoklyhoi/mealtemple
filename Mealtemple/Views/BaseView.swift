//
//  BaseView.swift
//  Mealtemple
//
//  Created by GIS on 12/5/19.
//  Copyright © 2019 GIS. All rights reserved.
//

import UIKit
class BaseView: UIView {
    
    public var delegate: handleClickDelegate!
    private var setView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let component = setComponent() {
            self.setView = component
            self.customInitView(view: &self.setView)
            self.addSubview(setView!)
        }
    }
    
    internal func setActionOnView(view: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(onClickView(sender:)))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
    }
    
    @objc internal func onClickView(sender: UITapGestureRecognizer) {
        //
    }
    
    internal func customInitView(view: inout UIView) {
        
    }
    
    internal func setComponent() -> UIView? {
        return nil
    }
    
    internal func getMaxYInView() -> CGFloat {
        return 0
    }
}
